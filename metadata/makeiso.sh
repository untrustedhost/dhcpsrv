#!/usr/bin/env bash

output=config.iso

here="$(pwd)"
scratch="$(mktemp -d)"

./mangle-example.sh > "${scratch}/MDDATA.XML"

(cd "${scratch}" && genisoimage -o "${here}/mddata.iso" -rational-rock -V IMD .)

rm -rf "${scratch}"

