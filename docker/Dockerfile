FROM 67d0bbde-4656-4493-81f2-fdfc16fe160e

# remove and purge conflicting packages first
RUN dpkg --purge clevis-dracut dracut-network iputils-arping

# install packages, configure them immediately
RUN /usr/lib/untrustedhost/scripts/pkginst.sh \
            isc-dhcp-relay dibbler-server \
            arping dhcping

# handle isc-dhcp-server as a source compile - debian/ubuntu is broken (May '22)
RUN sed -i -e '/^deb /p;s/deb /deb-src /g' /etc/apt/sources.list && \
    env DEBIAN_FRONTEND=noninteractive \
    bash -c 'apt-get update && \
      apt-get build-dep isc-dhcp-server && \
      apt-get clean all && \
      rm -rf /var/lib/apt/lists/*'

# retrieve/unpack is a separate layer so we can cache more easily
ADD isc-202122pgpkey /root/isc.asc
RUN gpg --import /root/isc.asc && \
    curl -L -o /tmp/dhcpd.tar.gz https://downloads.isc.org/isc/dhcp/4.4.3/dhcp-4.4.3.tar.gz && \
    curl -L -o /tmp/dhcpd.asc https://downloads.isc.org/isc/dhcp/4.4.3/dhcp-4.4.3.tar.gz.asc && \
    gpg --verify /tmp/dhcpd.asc /tmp/dhcpd.tar.gz && \
    mkdir /usr/src/dhcpd && \
    tar xf /tmp/dhcpd.tar.gz --strip-components 1 -C /usr/src/dhcpd && \
    rm -rf /tmp/dhcpd.*

# build it, install it
RUN cd /usr/src/dhcpd && ./configure && make && make install

# add systemd miscellanea
COPY systemd-system/ /etc/systemd/system/

RUN systemctl disable isc-dhcp-server.service && systemctl disable isc-dhcp-server6.service \
 && systemctl disable isc-dhcp-relay.service && systemctl disable isc-dhcp-relay6.service

RUN firewall-offline-cmd --zone public --add-service dhcp \
 && firewall-offline-cmd --direct --add-rule ipv4 filter INPUT   0 -p udp --sport 67 --dport 68 -j ACCEPT

# glue in a network namespace for dhcpd to party in
ADD scripts/* /usr/lib/untrustedhost/scripts/

# configure some more stuff for anycast-healthchecker
ADD anycast-dhcpd.conf /etc/anycast-healthchecker.d/dhcpd.conf

# add build stamps
ADD facts.d /usr/lib/untrustedhost/facts.d
RUN chmod 0644 /usr/lib/untrustedhost/facts.d/*
